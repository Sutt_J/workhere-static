# Static jobs page

This React application uses firebase functions to fetch places and jobs from a Realtime firebase database.

The `src` folder contains the source code for React app. It also uses [Webpack](https://github.com/webpack/webpack) to bundle the app and generate two bundles, one for the server, and one for the client. The server bundle is required by the express server to generate the initial markup, and the client bundle is included in the `functions/template.js` - the markup that's being passed onto the client.

## Setting up the app

 1. Create a Firebase Project using the [Firebase Console](https://console.firebase.google.com).
 1. Clone or download this repo and open the directory.
 1. You must have the Firebase CLI installed. If you don't have it install it with `npm install -g firebase-tools` and then configure it with `firebase login`.
 1. Configure the CLI locally by using `firebase use --add` and select your project in the list.
 1. Install dependencies locally by running: `cd functions; npm install; cd ../src; npm install`
 1. Run `npm run build` within the `src` folder to start webpack, which will bundle the app. It will output `functions/build/server.bundle.js` and `public/assets/client.bundle.js`
 1. Import the sample `functions/data-seed.json` to your Firebase Realtime Database. For more details, see [https://support.google.com/firebase/answer/6386780?hl=en#import](https://support.google.com/firebase/answer/6386780?hl=en#import)


## Deploy and test

This sample comes with a web-based UI for testing the function. To test it out:

 1. Deploy your project using `firebase deploy`
 1. Open the app using `firebase open hosting:site`, this will open a browser.



