

// Client and Server Data Fetching Logic
// Uses eitehr the client firebase (initialized from hosting init script)
// Or serverside firebase
const firebase = global.firebase || require('firebase');

// Initialize Firebase SDK
// Only should be called once on the server
// the client should already be initialized from hosting init script
const initializeApp = (config) => {
  if (firebase.apps.length === 0) {
    firebase.initializeApp(config);
  }
} 



const getAllPlaces = () => {
  return firebase.database().ref('/places').once('value').then((snap) => { 
    return { places: snap.val() };
  });
};


const getPlaceById = (placeId) => {
  return firebase.database().ref(`/places/${placeId}`).once('value').then((snap) => {
    const promises = [];
    const snapshot = snap.val();
   
    return firebase.Promise.all(promises).then((resp) => {
      return { currentPlace: { place: snapshot } };
    });
  });
};

module.exports = {
  getAllPlaces,
  getPlaceById,
  initializeApp
};
