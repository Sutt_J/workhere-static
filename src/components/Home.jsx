import React from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';

export default class Home extends React.Component {

  

  componentDidMount() {
    // if no data is present, load the places data
    if(_.isEmpty(this.props.places)) {
      this.props.getAllPlaces();
    }
  }

  render() {
    if(_.isEmpty(this.props.places)) {
      return <div className="container loader">Loading...</div>;
    }
    return (
      <div className='container home'>
        <ul className="cards">
          {_.reverse(_.sortBy(this.props.places, 'jobCount')).map((place) => {
            var n = (place.name || place.place.name);
            return (<li className="card card-inline" key={place._id}>
              <img className="card-img-top card-image" src={"https://firebasestorage.googleapis.com/v0/b/isomorphic-65fd3.appspot.com/o/logo.png?alt=media&token=3278e657-e493-402d-b796-34171c795272"}/>
              <div className="card-block">
                <h4 className="card-title">{n}</h4>
                <Link to={`/${place._id}`} className="btn">See Jobs</Link>
              </div>
            </li>)
          })}
        </ul>
      </div>
    );
  }
}
