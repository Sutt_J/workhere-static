

import React from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';

export default class Place extends React.Component {
  componentDidMount() {
    // if no data is present, load the place data from the database
    if(_.isEmpty(this.props.currentPlace) ||
      this.props.currentPlace.place._id !== this.props.match.params.id) {
      this.props.getPlaceById(this.props.match.params.id);
    }

  } 

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.id !== this.props.match.params.id) {
      // user has navigated to a new place details page
      // load data for that place and set to state
      this.props.getPlaceById(nextProps.match.params.id);
    }

  }

  
  render() {
    if(_.isEmpty(this.props.currentPlace)) {
      return <div className="container loader">Loading...</div>;
    }

    const { place } = this.props.currentPlace;
    var n = (place.name || place.place.name);
    return (
      <div className='user-details'>
        <div className='hero'>
          <div className='container'>
            <img className='employee-image' src={"https://firebasestorage.googleapis.com/v0/b/isomorphic-65fd3.appspot.com/o/logo.png?alt=media&token=3278e657-e493-402d-b796-34171c795272"} />
            <h1>{n}</h1>
          </div>
        </div>
        <div className='container'>
        <h4>Open Positions</h4>
          <table className='table table-striped details-table'>
            <tbody>
              <tr>
                <td className='title'>foo</td>
              </tr>
              <tr>
                <td className='title'>foo</td>
              </tr>
              <tr>
                <td className='title'>foo</td>
              </tr>
            </tbody>
          </table>
          
        </div>
      </div>
    )
  }
}
