import React from 'react';
import { Switch, Route } from 'react-router';
import { Link } from 'react-router-dom';
import Home from '../components/Home';
import Place from '../components/Place';
import database from 'firebase-database';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    // check to see if we have existing server-rendered data
    // sets the state if we do, otherwise initialize it to an empty state
    if (props.state) {
      this.state = props.state;
    } else { 
      this.state = {
        currentPlace: {},
        places: {},
      }
    }
  }

  getPlaceById = (placeId) => {
    this.setState({
      currenPlace: {}
    });
    database.getPlaceById(placeId).then(({currentPlace}) => {
      this.setState({
        currentPlace
      });
    });
  }


  getAllPlaces = () => {
    database.getAllPlaces().then(({places}) => {
      this.setState({
        places
      });
    });
  }



  render() {
    return (
      <div>
        <nav id="mainNav" className="navbar navbar-custom">
          <div className="container">
            <div className="navbar-header">
              <Link to='/' className="navbar-brand">Static Company Pages</Link>
            </div>
          </div>
        </nav>
        <Switch>
          <Route path='/:id' render={ (props) => (
                                        <Place {...props} 
                                        currentPlace = { this.state.currentPlace }
                                        getPlaceById = { this.getPlaceById }
                                        />
                                      ) } />
          <Route path='/' render={ (props) => (
                                     <Home {...props}
                                     places={ this.state.places } 
                                     getAllPlaces={ this.getAllPlaces } 
                                     />
                                   ) } />
        </Switch>
      </div>
    )
  }
}
